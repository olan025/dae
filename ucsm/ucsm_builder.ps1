Param(
  [Parameter(Mandatory=$True,Position=1)]
  [string]$datasource_name,
  [Parameter(Mandatory=$True,Position=2)]
  [string]$build_id
)

###### Set Constants
$psLoadModules = @("ciscoUcsPs")

###### Get Global Settings
$global_customers 	= $gdata.get_Item("global_customers")
$global_datacenter 	= $gdata.get_Item("global_datacenter")
$global_locations 	= $gdata.get_Item("global_locations")
$global_build_ids 	= $gdata.get_Item("global_build_ids")

$dc_dns				= $global_settings.dns_server_ips.split(",")

$customer_name = $global_customers.customer_name
$dc_dns1 = $dc_dns[0]
$dc_dns2 = $dc_dns[1]
$dc_search_domains = $global_datacenter.domain_name
$dc_ntp1 = $global_settings.dc_ntp1
$dc_ntp2 = $global_settings.dc_ntp2
$dc_timezone = $global_settings.dc_timezone
$smtp_relay = $global_settings.smtp_relay
$smtp_from_email_address = $global_settings.smtp_from_email_address
$smtp_to_email_address = $global_settings.smtp_to_email_address
$snmp_trap_reciever = $global_settings.snmp_trap_reciever
$snmp_community_string = $global_settings.snmp_community_string
$dc_syslog_1 = $global_settings.dc_syslog_1
$dc_syslog_2 = $global_settings.dc_syslog_2

###### Get UCS Datacenters
$ucsDatacenter = $ucsm_data.get_item("ucsm_datacenter")

$ucs_vip = $ucsDatacenter.ucs_vip
$ucs_fi_a_ip = $ucsDatacenter.ucs_fi_a_ip
$ucs_fi_b_ip = $ucsDatacenter.ucs_fi_b_ip
$ucs_fi_gateway = $ucsDatacenter.ucs_fi_gateway
$ucs_fi_subnet_mask = $ucsDatacenter.ucs_fi_subnet_mask
$ucs_kvm_ip_start = $ucsDatacenter.ucs_kvm_ip_start
$ucs_kvm_ip_end = $ucsDatacenter.ucs_kvm_ip_end
$ucs_kvm_gateway = $ucsDatacenter.ucs_kvm_gateway
$ucs_admin_user = $ucsDatacenter.ucs_admin_user
$ucs_admin_pass = $ucsDatacenter.ucs_admin_pass
$ucs_domain_name = $ucsDatacenter.ucs_domain_name
$ucs_site_num = $ucsDatacenter.ucs_site_num
$ucs_pod_num = $ucsDatacenter.ucs_pod_num
$ucs_connectivity_policy = $ucsDatacenter.ucs_connectivity_policy
$ucs_server_port_module = $ucsDatacenter.ucs_server_port_module
$ucs_server_port_start = [convert]::ToInt32($ucsDatacenter.ucs_server_port_start, 10)
$ucs_server_port_end = [convert]::ToInt32($ucsDatacenter.ucs_server_port_end, 10)
$ucs_uplink_port_module = $ucsDatacenter.ucs_uplink_port_module
$ucs_uplink_port_start = [convert]::ToInt32($ucsDatacenter.ucs_uplink_port_start, 10)
$ucs_uplink_port_end = [convert]::ToInt32($ucsDatacenter.ucs_uplink_port_end, 10)
$ucs_uplink_create_pc = $ucsDatacenter.ucs_uplink_create_pc
$ucs_uplink_vpc_id_a = $ucsDatacenter.ucs_uplink_vpc_id_a
$ucs_uplink_vpc_id_b = $ucsDatacenter.ucs_uplink_vpc_id_b
$ucs_fc_port_module = $ucsDatacenter.ucs_fc_port_module
$ucs_fc_port_start = [convert]::ToInt32($ucsDatacenter.ucs_fc_port_start, 10)
$ucs_fc_port_end = [convert]::ToInt32($ucsDatacenter.ucs_fc_port_end, 10)
$ucs_fc_vsan_id_a = $ucsDatacenter.ucs_fc_vsan_id_a
$ucs_fc_vsan_id_b = $ucsDatacenter.ucs_fc_vsan_id_b
$ucs_fc_use_fcoe = $ucsDatacenter.ucs_fc_use_fcoe
$ucs_fc_fcoe_vlan_a = $ucsDatacenter.ucs_fc_fcoe_vlan_a
$ucs_fc_fcoe_vlan_b = $ucsDatacenter.ucs_fc_fcoe_vlan_b
$ucs_fc_create_pc = $ucsDatacenter.ucs_fc_create_pc
$ucs_fc_pc_id_a = $ucsDatacenter.ucs_fc_pc_id_a
$ucs_fc_pc_id_b = $ucsDatacenter.ucs_fc_pc_id_b

######### Define Global Sections
$scriptOptions									= New-Object System.Collections.ArrayList
$scriptInputs									= New-Object System.Collections.ArrayList
$connectUcs										= New-Object System.Collections.ArrayList
$ucsGlobalPolicy								= New-Object System.Collections.ArrayList
$ucsFiPorts										= New-Object System.Collections.ArrayList
$ucsVsan										= New-Object System.Collections.ArrayList
$ucsLdap										= New-Object System.Collections.ArrayList
$ucsInitialCleanup								= New-Object System.Collections.ArrayList
$ucsRemoveDefaults								= New-Object System.Collections.ArrayList
$ucsFirmwarePackages   							= New-Object System.Collections.ArrayList
$ucsAllPools              						= New-Object System.Collections.ArrayList
$ucsQos                							= New-Object System.Collections.ArrayList
$ucsNetControlPolicy   							= New-Object System.Collections.ArrayList
$ucsVlans              							= New-Object System.Collections.ArrayList
$ucsVnicTemplates      							= New-Object System.Collections.ArrayList
$ucsVhbaTemplates      							= New-Object System.Collections.ArrayList
$ucsPolicies	       							= New-Object System.Collections.ArrayList
$ucsCallHome									= New-Object System.Collections.ArrayList
$ucsadauth										= New-Object System.Collections.ArrayList
$ucsSpTemplates        							= New-Object System.Collections.ArrayList
$ucsSvcProfiles        							= New-Object System.Collections.ArrayList
$outputScript									= @()
$outputConfig									= @()

######### Set script options
foreach ($psLoadModule in $psLoadModules) {
	[Void]$scriptOptions.Add("if ( (Get-PSSnapin -Name $psLoadModule -ErrorAction SilentlyContinue) -eq `$null ) {")
	[Void]$scriptOptions.Add("Add-PSSnapin $psLoadModule")
	[Void]$scriptOptions.Add("}")	
}

[Void]$scriptOptions.Add($carriage_return)
[Void]$scriptOptions.Add("`$ErrorActionPreference = ""Stop""")

[Void]$scriptOptions.Add($carriage_return)
[Void]$scriptOptions.Add("`$PSVersion = `$psversiontable.psversion")
[Void]$scriptOptions.Add("`$PSMinimum = `$PSVersion.Major")
[Void]$scriptOptions.Add("if (`$PSMinimum -lt ""3"") {
	Write-Output -ForegroundColor Red ""This script requires PowerShell version 3 or above, You are running version $PSVersion. Please update your system and try again.""
}")

[Void]$scriptOptions.Add($carriage_return)
[Void]$scriptOptions.Add("#Ensure all UCS Domains are disconnected")
[Void]$scriptOptions.Add("Disconnect-Ucs | Out-Null")

############### Gather Credentials
# Collect passwords needed during script runtime
if ($ucs_admin_pass) {
	$secureUcsAdminPassword = $ucs_admin_pass | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString
	[Void]$scriptInputs.Add("`$secureUcsAdminPassword = """ + $secureUcsAdminPassword + """ | ConvertTo-SecureString")
	[Void]$scriptInputs.Add("`$ucsUsername = ""admin""")
	[Void]$scriptInputs.Add("`$ucsCredentials = New-Object System.Management.Automation.PSCredential -ArgumentList `$ucsUsername, `$secureUcsAdminPassword")
} else {
	[Void]$scriptInputs.Add($carriage_return)
	[Void]$scriptInputs.Add("Write-Host ""`r""")
	[Void]$scriptInputs.Add("Write-Host -ForegroundColor Yellow ""Please enter the following information;"" ")
	[Void]$scriptInputs.Add($carriage_return)
	$passwordPrompt="UCS Administrator Password"
	[Void]$scriptInputs.Add("do {
		`$passwordCorrect=`$false
		`$ucsAdminPassword1 = Read-Host -Prompt ""Enter $passwordPrompt."" -AsSecureString
		`$ucsAdminPassword2 = Read-Host -Prompt ""Re-enter $passwordPrompt."" -AsSecureString
		if ([Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$ucsAdminPassword1)) -eq [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$ucsAdminPassword2))) {
			`$secureUcsAdminPassword = `$ucsAdminPassword1
			`$passwordCorrect=`$true
			`$ucsUsername = ""root""
			`$ucsCredentials = New-Object System.Management.Automation.PSCredential -ArgumentList `$ucsUsername, `$secureUcsAdminPassword
		} else {
			Write-Host -ForegroundColor Red ""$passwordPrompt passwords did not match. Please re-enter.""
		}
	} while (`$passwordCorrect -ne `$true)")
}

############### Connect to UCS Domain
[Void]$connectUcs.Add($carriage_return)
[Void]$connectUcs.Add("Write-Output ""Logging into UCS Domain""")
[Void]$connectUcs.Add("`$myCon = $null")
[Void]$connectUcs.Add("`$myCon = Connect-Ucs $ucs_vip -Credential `$ucsCredentials")
[Void]$connectUcs.Add("sleep 1")
[Void]$connectUcs.Add("`$myCon = (Get-UcsPSSession | measure).Count")
[Void]$connectUcs.Add("if (`$myCon -eq 0) {
	Write-Output -ForegroundColor Red ""Logon to UCS domain failed. Please check its availability and try again.""
	Disconnect-Ucs
	stop
}")

############### Set UCS connectivity policy
[Void]$ucsGlobalPolicy.Add($carriage_return)
[Void]$ucsGlobalPolicy.Add("# Set Global System Policies")
[Void]$ucsGlobalPolicy.Add("Start-UcsTransaction")
if ($ucs_connectivity_policy) {
	switch ($ucs_connectivity_policy) {
		4 { $ucsConnPolicy = "4-link"
		break;
		}
		2 {$ucsConnPolicy = "2-link"
		break;
		}
		1 {$ucsConnPolicy = "1-link"
		break;
		}
	}
	[Void]$ucsGlobalPolicy.Add("`$connPolicy = Get-UcsChassisDiscoveryPolicy | Set-UcsChassisDiscoveryPolicy -Action $ucsConnPolicy -LinkAggregationPref port-channel -Rebalance user-acknowledged -force")
}
[Void]$ucsGlobalPolicy.Add("`$powerPolicy = Get-UcsPowerControlPolicy | Set-UcsPowerControlPolicy -Redundancy grid -force")
[Void]$ucsGlobalPolicy.Add("`$macAging = Get-UcsLanCloud | Set-UcsLanCloud -macaging mode-default -force")
[Void]$ucsGlobalPolicy.Add("`$powerMgmtPolicy = Get-UcsPowerMgmtPolicy | Set-UcspowerMgmtPolicy -Style manual-per-blade -force")
[Void]$ucsGlobalPolicy.Add("`$ucsDns_1 = Add-UcsDnsServer -Name $dc_dns1")
[Void]$ucsGlobalPolicy.Add("`$ucsDns_2 = Add-UcsDnsServer -Name $dc_dns2")
[Void]$ucsGlobalPolicy.Add("`$ucsTimezone = Set-UcsTimezone -Timezone ""$dc_timezone"" -Force")
[Void]$ucsGlobalPolicy.Add("`$ucsNtp_1 = Add-UcsNtpServer -Name $dc_ntp1")
if($dc_ntp2){
	[Void]$ucsGlobalPolicy.Add("`$ucsNtp_2 = Add-UcsNtpServer -Name $dc_ntp2")
}
[Void]$ucsGlobalPolicy.Add("`Complete-UcsTransaction")

############### Configure UCS FI Ports & VSANs
# UCS Server Ports
if (($ucs_server_port_module) -And ($ucs_server_port_start) -And ($ucs_server_port_end)) {
	[Void]$ucsFiPorts.Add($carriage_return)
	[Void]$ucsFiPorts.Add("Write-Host ""Configuring UCS Server Ports""")	
	[Void]$ucsFiPorts.Add("Start-UcsTransaction")
	$counter=1
	for ($i=$ucs_server_port_start; $i -le $ucs_server_port_end; $i++) {
		[Void]$ucsFiPorts.Add("`$mo_$counter = Get-UcsFabricServerCloud -Id ""A"" | Add-UcsServerPort -AdminState ""enabled"" -PortId $i -SlotId $ucs_server_port_module")
		$counter++
	}
	[Void]$ucsFiPorts.Add("Complete-UcsTransaction")

	[Void]$ucsFiPorts.Add("Start-UcsTransaction")
	$counter=1
	for ($i=$ucs_server_port_start; $i -le $ucs_server_port_end; $i++) {
		[Void]$ucsFiPorts.Add("`$mo_$counter = Get-UcsFabricServerCloud -Id ""B"" | Add-UcsServerPort -AdminState ""enabled"" -PortId $i -SlotId $ucs_server_port_module")
		$counter++
	}
	[Void]$ucsFiPorts.Add("Complete-UcsTransaction")
	$counter=$null
}

# Ethernet Uplink Ports
if (($ucs_uplink_port_module) -And ($ucs_uplink_port_start) -And ($ucs_uplink_port_end)) {
	[Void]$ucsFiPorts.Add($carriage_return)
	[Void]$ucsFiPorts.Add("Write-Host ""Configuring UCS Ethernet Uplink Ports""")	
	# If uplinks are in a port channels, create the port channel & assign uplink onto it
	if ($ucs_uplink_create_pc="yes") {
		$counter=1
		[Void]$ucsFiPorts.Add("Start-UcsTransaction")
		[Void]$ucsFiPorts.Add("`$mo = Get-UcsFiLanCloud -Id ""A"" | Add-UcsUplinkPortChannel -AdminState ""enabled"" -Name (""vPC"" + $ucs_uplink_vpc_id_a) -PortId $ucs_uplink_vpc_id_a")
		for ($i=$ucs_uplink_port_start; $i -le $ucs_uplink_port_end; $i++) {
			[Void]$ucsFiPorts.Add("`$mo_$counter = `$mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState ""enabled"" -PortId $i -SlotId $ucs_uplink_port_module")
			$counter++
		}
		[Void]$ucsFiPorts.Add("Complete-UcsTransaction")
		
		$counter=1
		[Void]$ucsFiPorts.Add("Start-UcsTransaction")
		[Void]$ucsFiPorts.Add("`$mo = Get-UcsFiLanCloud -Id ""B"" | Add-UcsUplinkPortChannel -AdminState ""enabled"" -Name (""vPC"" + $ucs_uplink_vpc_id_b) -PortId $ucs_uplink_vpc_id_b")
		for ($i=$ucs_uplink_port_start; $i -le $ucs_uplink_port_end; $i++) {
			[Void]$ucsFiPorts.Add("`$mo_$counter = `$mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState ""enabled"" -PortId $i -SlotId $ucs_uplink_port_module")
			$counter++
		}
		[Void]$ucsFiPorts.Add("Complete-UcsTransaction")
	# If uplinks are not in a port channels, create the uplinks
	} else {
		[Void]$ucsFiPorts.Add("Start-UcsTransaction")
		for ($i=$ucs_uplink_port_start; $i -le $ucs_uplink_port_end; $i++) {
			[Void]$ucsFiPorts.Add("`$mo_$counter = Get-UcsFiLanCloud -Id ""A"" | Add-UcsUplinkPort -portid $i -slotid $ucs_uplink_port_module")
			$counter++
		}
		[Void]$ucsFiPorts.Add("Complete-UcsTransaction")
		
		$counter=1
		[Void]$ucsFiPorts.Add("Start-UcsTransaction")
		for ($i=$ucs_uplink_port_start; $i -le $ucs_uplink_port_end; $i++) {
			[Void]$ucsFiPorts.Add("`$mo_$counter = Get-UcsFiLanCloud -Id ""B"" | Add-UcsUplinkPort -portid $i -slotid $ucs_uplink_port_module")
			$counter++
		}
		[Void]$ucsFiPorts.Add("Complete-UcsTransaction")
	}
	$counter=$null
}

# FC Uplink Ports
if (($ucs_fc_port_module) -And ($ucs_fc_port_start) -And ($ucs_fc_port_end)) {
	[Void]$ucsFiPorts.Add($carriage_return)
	[Void]$ucsFiPorts.Add("Write-Host ""Configuring UCS FC Uplink Ports""")	
	if ($ucs_uplink_create_pc="yes") {
		$counter=1
		[Void]$ucsFiPorts.Add("Start-UcsTransaction")
		[Void]$ucsFiPorts.Add("`$moA = Get-UcsFiSanCloud -Id ""A"" | Add-UcsFcUplinkPortChannel -AdminState ""enabled"" -Name (""sanPC"" + $ucs_fc_pc_id_a) -PortId $ucs_fc_pc_id_a")
		for ($i=$ucs_fc_port_start; $i -le $ucs_fc_port_end; $i++) {
			[Void]$ucsFiPorts.Add("`$mo_$counter = `$moA | Add-UcsFabricFcSanPcEp -ModifyPresent -AdminState ""enabled"" -PortId $i -SlotId $ucs_fc_port_module")
			$counter++
		}
		[Void]$ucsFiPorts.Add("`$addVsanPort = Get-UcsFiSanCloud -Id ""A"" | Get-UcsVsan -Name $ucs_fc_vsan_id_a | Add-UcsVsanMemberFcPortChannel -ModifyPresent -AdminState ""enabled"" -PortId $ucs_fc_pc_id_a -SwitchId A")
		[Void]$ucsFiPorts.Add("Complete-UcsTransaction")
		
		$counter=1
		[Void]$ucsFiPorts.Add("Start-UcsTransaction")
		[Void]$ucsFiPorts.Add("`$moB = Get-UcsFiSanCloud -Id ""B"" | Add-UcsFcUplinkPortChannel -AdminState ""enabled"" -Name (""sanPC"" + $ucs_fc_pc_id_b) -PortId $ucs_fc_pc_id_b")
		for ($i=$ucs_fc_port_start; $i -le $ucs_fc_port_end; $i++) {
			[Void]$ucsFiPorts.Add("`$mo_$counter = `$moB | Add-UcsFabricFcSanPcEp -ModifyPresent -AdminState ""enabled"" -PortId $i -SlotId $ucs_fc_port_module")
			$counter++
		}
		[Void]$ucsFiPorts.Add("`$addVsanPort = Get-UcsFiSanCloud -Id ""B"" | Get-UcsVsan -Name $ucs_fc_vsan_id_b | Add-UcsVsanMemberFcPortChannel -ModifyPresent -AdminState ""enabled"" -PortId $ucs_fc_pc_id_b -SwitchId B")
		[Void]$ucsFiPorts.Add("Complete-UcsTransaction")
	} else {
		[Void]$ucsFiPorts.Add("Start-UcsTransaction")
		for ($i=$ucs_fc_port_start; $i -le $ucs_fc_port_end; $i++) {
			[Void]$ucsFiPorts.Add("`$mo_$counter = Get-UcsFiSanCloud -Id ""A"" | Add-UcsFcUplinkPort -ModifyPresent -AdminState ""enabled"" -PortId $i -SlotId $ucs_fc_port_module")
			$counter++
		}
		[Void]$ucsFiPorts.Add("Complete-UcsTransaction")
		[Void]$ucsFiPorts.Add("Start-UcsTransaction")
		for ($i=$ucs_fc_port_start; $i -le $ucs_fc_port_end; $i++) {
			[Void]$ucsFiPorts.Add("`$mo_$counter = Get-UcsFiSanCloud -Id ""B"" | Add-UcsFcUplinkPort -ModifyPresent -AdminState ""enabled"" -PortId $i -SlotId $ucs_fc_port_module")
			$counter++
		}
		[Void]$ucsFiPorts.Add("Complete-UcsTransaction")
	}
	$counter=$null
}

# Create VSANs #####
if (($ucs_fc_vsan_id_a) -And ($ucs_fc_vsan_id_b)) {
	[Void]$ucsVsan.Add("Start-UcsTransaction")
	if ($ucs_fc_use_fcoe -eq "yes") {
		[Void]$ucsVsan.Add("Get-UcsFiSanCloud -Id ""A"" | Add-UcsVsan -FcoeVlan $ucs_fc_fcoe_vlan_a -Id $ucs_fc_vsan_id_a -Name $ucs_fc_vsan_id_a")
		[Void]$ucsVsan.Add("Get-UcsFiSanCloud -Id ""B"" | Add-UcsVsan -FcoeVlan $ucs_fc_fcoe_vlan_b -Id $ucs_fc_vsan_id_b -Name $ucs_fc_vsan_id_b")
	} else {
		[Void]$ucsVsan.Add("Get-UcsFiSanCloud -Id ""A"" | Add-UcsVsan -FcoeVlan $ucs_fc_vsan_id_a -Id $ucs_fc_vsan_id_a -Name $ucs_fc_vsan_id_a")
		[Void]$ucsVsan.Add("Get-UcsFiSanCloud -Id ""B"" | Add-UcsVsan -FcoeVlan $ucs_fc_vsan_id_b -Id $ucs_fc_vsan_id_b -Name $ucs_fc_vsan_id_b")
	}
	[Void]$ucsVsan.Add("Complete-UcsTransaction")
}

############### Initial Clean-up of unneeded UCS Objects
[Void]$ucsInitialCleanup.Add($carriage_return)
[Void]$ucsInitialCleanup.Add("Write-Host ""Performing initial cleanup""")
[Void]$ucsInitialCleanup.Add("Start-UcsTransaction")
[Void]$ucsInitialCleanup.Add("Get-UcsServerPool | Remove-UcsServerPool -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsUuidSuffixPool | Remove-UcsUuidSuffixPool -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsMacPool | Remove-UcsMacPool -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsVnicTemplate | Remove-UcsVnicTemplate -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsIpPool | Where-Object {`$_.name -ne ""ext-mgmt""} | Remove-UcsIpPool -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsSanCloud | Get-UcsVsan | Where-Object {`$_.name -ne ""default""} | Remove-UcsVsan -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsServiceProfile | Remove-UcsServiceProfile -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsLdapProvider | Remove-UcsLdapProvider -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsProviderGroup | Remove-UcsProviderGroup -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsLanCloud | Get-UcsVlan | Where-Object {`$_.name -ne ""default""} | Remove-UcsVlan -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsVhbaTemplate | Remove-UcsVhbaTemplate -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsPowerPolicy | Remove-UcsPowerPolicy -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsMaintenancePolicy | Where-Object {`$_.name -ne ""default""} | Remove-UcsMaintenancePolicy -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsLocalDiskConfigPolicy | Remove-UcsLocalDiskConfigPolicy -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsBootPolicy | Remove-UcsBootPolicy -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsBiosPolicy | Remove-UcsBiosPolicy -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsQosPolicy | Remove-UcsQosPolicy -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsOrg | Where-Object {`$_.Name -ne ""root""} | Remove-UcsOrg -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsDnsServer | Remove-UcsDnsServer -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsNtpServer | Remove-UcsNtpServer -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsNetworkControlPolicy | Remove-UcsNetworkControlPolicy -Force")
[Void]$ucsInitialCleanup.Add("Get-UcsWwnPool | Remove-UcsWwnPool -Force")
[Void]$ucsInitialCleanup.Add("Complete-UcsTransaction")


############### Remove Default UCS Objects
[Void]$ucsRemoveDefaults.Add($carriage_return)
[Void]$ucsRemoveDefaults.Add("Write-Host ""Cleaning up UCS default values and pools""")
[Void]$ucsRemoveDefaults.Add("Start-UcsTransaction")
[Void]$ucsRemoveDefaults.Add("Get-UcsServerPool -Name default -LimitScope | Remove-UcsServerPool -Force")
[Void]$ucsRemoveDefaults.Add("Get-UcsUuidSuffixPool -Name default -LimitScope | Remove-UcsUuidSuffixPool -Force")
[Void]$ucsRemoveDefaults.Add("Get-UcsWwnPool -Name node-default -LimitScope | Remove-UcsWwnPool -Force")
[Void]$ucsRemoveDefaults.Add("Get-UcsWwnPool -Name default -LimitScope | Remove-UcsWwnPool -Force")
[Void]$ucsRemoveDefaults.Add("Get-UcsMacPool -Name default -LimitScope | Remove-UcsMacPool -Force")
[Void]$ucsRemoveDefaults.Add("Get-UcsManagedObject -Dn org-root/iqn-pool-default | Remove-UcsManagedObject -Force")
[Void]$ucsRemoveDefaults.Add("Complete-UcsTransaction")

############### Create UCS Firmware Packages
[Void]$ucsFirmwarePackages.Add($carriage_return)

############### Create UCS CIMC, UUID, WWNN, WWPN, MAC, IP CIMC and iSCSI pools
$uuidname = "UUID"
$macPrefix = "00:25:B5"
$wwnnPrefix = "20:00:00:25:B5"
$wwpnPrefix = "20:00:00:25:B5"

if ($ucs_site_num.Length -eq 1) {
	$ucsSiteNum = "0" + $ucs_site_num
} else {
	$ucsSiteNum = $ucs_site_num
}

if ($ucs_pod_num.Length -eq 1) {
	$ucsPodNum = "0" + $ucs_pod_num
} else {
	$ucsPodNum = $ucs_pod_num
}

$uuidfrom = $ucsSiteNum + $ucsPodNum + "-000000000001"
$uuidto= $ucsSiteNum + $ucsPodNum + "-000000000200"
$wwnnFrom = $wwnnPrefix+":"+$ucsSiteNum+":"+$ucsPodNum+":00" 
$wwnnTo = $wwnnPrefix+":"+$ucsSiteNum+":"+$ucsPodNum+":FF"

[Void]$ucsAllPools.Add($carriage_return)
[Void]$ucsAllPools.Add("Start-UcsTransaction")
$counter=1
$ucsPools = $cdata.get_item("ucs_pools")
foreach ($ucsPool in $ucsPools) {
	$ucs_pool_name = $ucsPool.ucs_pool_name
	$ucs_pool_fabric = $ucsPool.ucs_pool_fabric
	$ucs_pool_type = $ucsPool.ucs_pool_type
	$ucs_pool_custom_postfix = $ucsPool.ucs_pool_custom_postfix

	switch ($ucs_pool_type) {
		"mac" {
			$macFrom = $macPrefix+":"+$ucs_pool_custom_postfix.ToUpper()+":00"
			$macTo = $macPrefix+":"+$ucs_pool_custom_postfix.ToUpper()+":FF"
			[Void]$ucsAllPools.Add("`$mac$counter = Get-UcsOrg -Level root | Add-UcsMacPool -AssignmentOrder ""sequential"" -Descr ""MAC Address Pool on $ucs_pool_name"" -Name $ucs_pool_name")
			[Void]$ucsAllPools.Add("`$mac$counter_1 = `$mac$counter | Add-UcsMacMemberBlock -From ""$macFrom"" -To ""$macTo"" ")
		}
		"wwpn" {
			$wwpnFrom = $wwpnPrefix+":"+$ucs_pool_custom_postfix.ToUpper()+":00"
			$wwpnTo = $wwpnPrefix+":"+$ucs_pool_custom_postfix.ToUpper()+":FF"
			[Void]$ucsAllPools.Add("`$wwpn$counter = Get-UcsOrg -Level root | Add-UcsWwnPool -AssignmentOrder ""sequential"" -Descr ""WWPN Pool on fabric $ucs_pool_name"" -Name $ucs_pool_name -Purpose port-wwn-assignment")
			[Void]$ucsAllPools.Add("`$wwpn$counter_1 = `$wwpn$counter | Add-UcsWwnMemberBlock -From ""$wwpnFrom"" -To ""$wwpnTo"" ")
		}
	}
	$counter++
}
[Void]$ucsAllPools.Add("`$wwnn1 = Get-UcsOrg -Level root | Add-UcsWwnPool -AssignmentOrder ""sequential"" -Descr ""WWNN Pool"" -Name ""Global"" -Purpose node-wwn-assignment")
[Void]$ucsAllPools.Add("`$wwnn1_1 = `$wwnn1 | Add-UcsWwnMemberBlock -From ""$wwpnFrom"" -To ""$wwpnTo"" ")
[Void]$ucsAllPools.Add("`$uuid1 = Get-UcsOrg -Level root  | Add-UcsUuidSuffixPool -AssignmentOrder ""sequential"" -Descr ""UUID Pool for Server system board IDs"" -Name $uuidname -Prefix derived")
[Void]$ucsAllPools.Add("`$uuid1_1 = `$uuid1 | Add-UcsUuidSuffixBlock -From $uuidfrom -To $uuidto")
[Void]$ucsAllPools.Add("Get-UcsIpPool -org root -Name ext-mgmt -LimitScope | Add-UcsIpPoolBlock -DefGw $ucs_kvm_gateway -From $ucs_kvm_ip_start -To $ucs_kvm_ip_end")
[Void]$ucsAllPools.Add("Complete-UcsTransaction")

############### Configure UCS QoS
$ucsQosPolicies = $cdata.get_item("ucs_qos")
[Void]$ucsQos.Add($carriage_return) 
if (($ucs_fc_port_module) -And ($ucs_fc_port_start) -And ($ucs_fc_port_end)) {
	[Void]$ucsQos.Add("Start-UcsTransaction")
	[Void]$ucsQos.Add("Set-UcsQosClass -QosClass gold -Weight 4 -Mtu 9216 -AdminState enabled -Force")
	[Void]$ucsQos.Add("Set-UcsQosClass -QosClass silver -Weight 3 -mtu 9216 -AdminState enabled -Force")
	[Void]$ucsQos.Add("Set-UcsQosClass -QosClass bronze -Weight 2 -mtu 9216 -AdminState enabled -Force")
	[Void]$ucsQos.Add("Set-UcsBestEffortQosClass -Weight 1 -mtu 9216 -Force")
	[Void]$ucsQos.Add("Set-UcsFcQosClass -Weight 0 -Force")
	[Void]$ucsQos.Add("Complete-UcsTransaction")
} else {
	[Void]$ucsQos.Add("Start-UcsTransaction")
	[Void]$ucsQos.Add("Set-UcsQosClass -QosClass gold -Weight 4 -Mtu 9216 -AdminState enabled -Force")
	[Void]$ucsQos.Add("Set-UcsQosClass -QosClass silver -Weight 3 -mtu 9216 -AdminState enabled -Force")
	[Void]$ucsQos.Add("Set-UcsQosClass -QosClass bronze -Weight 2 -mtu 9216 -AdminState enabled -Force")
	[Void]$ucsQos.Add("Set-UcsBestEffortQosClass -Weight 1 -mtu 9216 -Force")
	[Void]$ucsQos.Add("Set-UcsFcQosClass -Weight 0 -Force")
	[Void]$ucsQos.Add("Complete-UcsTransaction")
}

[Void]$ucsQos.Add($carriage_return)
[Void]$ucsQos.Add("Start-UcsTransaction")
$counter=1
foreach ($ucsQosPolicy in $ucsQosPolicies) {
	$ucs_qos_name = $ucsQosPolicy.ucs_qos_name
	$ucs_qos_burst = $ucsQosPolicy.ucs_qos_burst
	$ucs_qos_host_control = $ucsQosPolicy.ucs_qos_host_control
	$ucs_qos_priority = $ucsQosPolicy.ucs_qos_priority
	$ucs_qos_rate = $ucsQosPolicy.ucs_qos_rate
	[Void]$ucsQos.Add("`$mo$counter = Get-UcsOrg -Level root | Add-UcsQosPolicy -Name ""$ucs_qos_name""")
	[Void]$ucsQos.Add("`$mo$counter_1 = `$mo$counter | Add-UcsVnicEgressPolicy -ModifyPresent -Burst $ucs_qos_burst -HostControl $ucs_qos_host_control -Prio $ucs_qos_priority -Rate $ucs_qos_rate")
	$counter++
}
[Void]$ucsQos.Add("Complete-UcsTransaction")

############### Configure UCS Network Control Policy
[Void]$ucsNetControlPolicy.Add($carriage_return) 
[Void]$ucsNetControlPolicy.Add("Start-UcsTransaction") 
[Void]$ucsNetControlPolicy.Add("`$mo = Get-UcsOrg -Level root | Add-UcsNetworkControlPolicy -Cdp ""enabled"" -Descr """" -MacRegisterMode ""only-native-vlan"" -Name ""CDP-Enabled"" -PolicyOwner ""local"" -UplinkFailAction ""link-down""") 
[Void]$ucsNetControlPolicy.Add("`$mo_1 = `$mo | Add-UcsPortSecurityConfig -ModifyPresent -Descr """" -Forge ""allow"" -Name """" -PolicyOwner ""local""") 
[Void]$ucsNetControlPolicy.Add("Complete-UcsTransaction") 

############### Create UCS VLANs
$ucsNetworkVlan = $cdata.get_item("ucs_vlans")
[Void]$ucsVlans.Add($carriage_return)
[Void]$ucsVlans.Add("Write-Host ""Creating UCS VLANs""")
[Void]$ucsVlans.Add("Start-UcsTransaction")

foreach ($vlan in $ucsNetworkVlan) {
	$ucs_vlan_name = $vlan.ucs_vlan_name
	$ucs_vlanid = $vlan.ucs_vlan
	[Void]$ucsVlans.Add("Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id $ucs_vlanid -Name $ucs_vlan_name")
}
[Void]$ucsVlans.Add("Complete-UcsTransaction") 

############### Create UCS vNic Templates
$ucs_network_vlan = $cdata.get_item("ucs_networks_vlan")
$ucsnetworks = $cdata.get_item("ucs_networks")
[Void]$ucsVnicTemplates.Add($carriage_return)
[Void]$ucsVnicTemplates.Add("Write-Host ""Creating UCS vNic Templates""")

Foreach($vNic in $ucsnetworks){
	$vnic_descr = $vNic.ucs_vnic_description
	$vnic_mac_pool = $vNic.ucs_nic_mac_pool_prefix
	$vnic_name = $vNic.ucs_vnic_name
	$vnic_QOS = $vNic.ucs_qos_policy
	$vnic_fabric = $vNic.ucs_vnic_fabric
	$vnic_vlan = $vNic.ucs_vnic_vlan_descr
	[Void]$ucsVnicTemplates.Add("Start-UcsTransaction")
	[Void]$ucsVnicTemplates.Add("`$mo$counter = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr $vnic_descr -IdentPoolName $vnic_mac_pool -Mtu 9000 -Name $vnic_name -NwCtrlPolicyName ""Enable_CDP"" -PinToGroupName """" -PolicyOwner ""local"" -QosPolicyName $vnic_QOS -StatsPolicyName ""default"" -SwitchId $vnic_fabric -TemplType ""updating-template""")
	[Void]$ucsVnicTemplates.Add("`$mo_1 = `$mo$counter | Add-UcsVnicInterface -ModifyPresent -DefaultNet ""no"" -Name $vnic_vlan")
	[Void]$ucsVnicTemplates.Add("Complete-UcsTransaction")	
}

[Void]$ucsVnicTemplates.Add($carriage_return)
[Void]$ucsVnicTemplates.Add("Start-UcsTransaction")

Foreach ($vlan in $ucs_network_vlan){
	$vnicname = $vlan.ucs_vnic_name
	$vlanname = $vlan.ucs_vlan_name
	
	[Void]$ucsVnicTemplates.Add("Add-UcsVnicInterface -VnicTemplate $vnicname -ModifyPresent -DefaultNet no -Name $vlanname")
}
[Void]$ucsVnicTemplates.Add("Complete-UcsTransaction")

############### Create UCS vHBA Templates
[Void]$ucsVhbaTemplates.Add($carriage_return)
$ucs_hba = $cdata.get_item("ucs_hba")

[Void]$ucsVhbaTemplates.Add("Start-UcsTransaction")
Foreach ($vhbaid in $ucs_hba){
	$vhbaname = $vhbaid.ucs_hba_name
	$wwpn_pool = $vhbaid.ucs_wwpn_name
	$ucs_hba_descr = $vhbaid.ucs_hba_description
	$ucs_hba_vsan = $vhbaid.ucs_hba_vsan
	
	[Void]$ucsVhbaTemplates.Add("`$mo = Get-UcsOrg -Level root  | Add-UcsVhbaTemplate -Descr ""$ucs_hba_descr"" -IdentPoolName ""Global"" -MaxDataFieldSize 2048 -Name $vhbaname -QosPolicyName ESXi_FC -StatsPolicyName default -SwitchId A -TemplType updating-template")
	[Void]$ucsVhbaTemplates.Add("`$mo_1 = `$mo | Add-UcsVhbaInterface -ModifyPresent -Name $ucs_hba_vsan")
}
[Void]$ucsVhbaTemplates.Add("Complete-UcsTransaction")

############### Configure UCS Policies
[Void]$ucsPolicies.Add($carriage_return)
[Void]$ucsPolicies.Add("Write-Host ""Configuring UCS Policies""")
[Void]$ucsPolicies.Add("Get-UcsOrg -Level root  | Add-UcsLocalDiskConfigPolicy -Descr """" -FlexFlashRAIDReportingState ""enable"" -FlexFlashState ""enable"" -Mode ""any-configuration"" -Name ""SD"" -PolicyOwner ""local"" -ProtectConfig ""yes""")
[Void]$ucsPolicies.Add("Get-UcsOrg -Level root  | Add-UcsLocalDiskConfigPolicy -Descr """" -FlexFlashRAIDReportingState ""disable"" -FlexFlashState ""disable"" -Mode ""raid-mirrored"" -Name ""Local_Disk"" -PolicyOwner ""local"" -ProtectConfig ""yes""")
[Void]$ucsPolicies.Add("Get-UcsOrg -Level root | Get-UcsMaintenancePolicy -Name ""default"" -LimitScope | Set-UcsMaintenancePolicy -Descr """" -SchedName """" -UptimeDisr ""user-ack"" -Force")
[Void]$ucsPolicies.Add("Get-UcsOrg -Level root | Add-UcsMaintenancePolicy -Descr """" -Name ""User-Acknowledge"" -SchedName """" -UptimeDisr ""user-ack""")
[Void]$ucsPolicies.Add("Get-UcsOrg -Level root | Get-UcsScrubPolicy -Name ""default"" -LimitScope | Set-UcsScrubPolicy -BiosSettingsScrub ""yes"" -Descr """" -DiskScrub ""no"" -FlexFlashScrub ""no"" -PolicyOwner ""local"" -Force")
[Void]$ucsPolicies.Add("Get-UcsOrg -Level root  | Add-UcsScrubPolicy -BiosSettingsScrub ""yes"" -Descr """" -DiskScrub ""no"" -FlexFlashScrub ""no"" -Name ""Bios-Only"" -PolicyOwner ""local""")


############### Configure Call Home
[Void]$ucsCallHome.Add($carriage_return)
[Void]$ucsCallHome.Add("Write-Host ""Configuring Call Home""")
#$callhome_smtp	 			= 
#$callhome_addr				= 
#$callhome_contact			=
#$callhome_contract			=
#$callhome_customer			=
#$callhome_email				=
#$callhome_from				=
#$callhome_replyto			=
#$callhome_phone				=
#$callhome_site				=

#[Void]$ucsPolicies.Add("Start-UcsTransaction")
#[Void]$ucsPolicies.Add("`$mo = Get-UcsCallhome | Set-UcsCallhome -AdminState ""on"" -AlertThrottlingAdminState ""on"" -Descr """" -Name """" -PolicyOwner ""local""")
#[Void]$ucsPolicies.Add("`$mo_1 = Get-UcsCallhomeSmtp | Set-UcsCallhomeSmtp -Host "$callhome_smtp" -Port 25")
#[Void]$ucsPolicies.Add("`$mo_2 = Get-UcsCallhomeSource | Set-UcsCallhomeSource -Addr "$callhome_addr" -Contact "$callhome_contact" -Contract "$callhome_contract" -Customer "$callhome_customer" -Email "$callhome_email" -From "$callhome_from" -Phone "$callhome_phone" -ReplyTo "$callhome_replyto" -Site "$callhome_site" -Urgency ""debug""")
#[Void]$ucsPolicies.Add("Complete-UcsTransaction")


############### Configure AD Auth
[Void]$ucsadauth.Add($carriage_return)
[Void]$ucsadauth.Add("Write-Host ""Configuring AD Authentication""")

$ucsad_auth = $cdata.get_item("ucs_ad_auth")
$ucs_auth_ldap_provider_group_name = $ucsad_auth.ucs_auth_ldap_provider_group_name
$ucs_auth_ldap_provider_group_included_providers = $ucsad_auth.ucs_auth_ldap_provider_group_included_providers
$ucs_auth_domain_ldap_name = $ucsad_auth.ucs_auth_domain_ldap_name
$ucs_auth_domain_ldap_provider_group = $ucsad_auth.ucs_auth_domain_ldap_provider_group
$ucs_auth_native_default_provider_group = $ucsad_auth.ucs_auth_native_default_provider_group
$ucs_auth_domain_basedn = $ucsad_auth.ucs_auth_domain_basedn
$ucs_auth_domain_binddn	= $ucsad_auth.ucs_auth_domain_binddn
$ucs_auth_domain = $ucs_auth_domain_ldap_name.split(".")
$ucs_auth_domain_name = $ucs_auth_domain[0]

[Void]$ucsadauth.Add("Start-UcsTransaction")
[Void]$ucsadauth.Add("Set-UcsLdapGlobalConfig -Attribute """" -Basedn ""$ucs_auth_domain_basedn"" -Descr """" -FilterValue ""sAMAccountName=$userid"" -Retries 1 -Timeout 30 -Force")
[Void]$ucsadauth.Add("`$mo = Add-UcsLdapProvider -Attribute """" -Basedn ""$ucs_auth_domain_basedn"" -Descr """" -EnableSSL ""no"" -FilterValue """" -Key ""7b56Yc3t"" -Name ""$ucs_auth_domain_ldap_name"" -Order ""lowest-available"" -Port 389 -Retries 1 -Rootdn ""$ucs_auth_domain_binddn"" -Timeout 30 -vendor ""MS-AD""")
[Void]$ucsadauth.Add("`$mo_1 = `$mo | Add-UcsLdapGroupRule -ModifyPresent -Authorization ""enable"" -Descr """" -TargetAttr ""memberOf"" -Traversal ""recursive""")
[Void]$ucsadauth.Add("`$mo = Get-UcsLdapGlobalConfig | Add-UcsProviderGroup -Descr """" -Name ""$ucs_auth_domain_ldap_name""")
[Void]$ucsadauth.Add("`$mo_1 = `$mo | Add-UcsProviderReference -ModifyPresent -Descr """" -Name ""$ucs_auth_domain_ldap_name"" -Order ""1""")
[Void]$ucsadauth.Add("`$mo_ = Add-UcsLdapGroupMap -Descr """" -Name ""$ucs_auth_ldap_provider_group_included_providers""")
[Void]$ucsadauth.Add("`$mo_1 = `$mo | Add-UcsUserRole -Descr """" -Name ""admin""")
[Void]$ucsadauth.Add("`$mo = Add-UcsAuthDomain -Descr """" -Name ""$ucs_auth_domain_name"" -RefreshPeriod 600 -SessionTimeout 7200 -Force")
[Void]$ucsadauth.Add("`$mo_1 = `$mo | Set-UcsAuthDomainDefaultAuth -Descr """" -Name """" -ProviderGroup ""$ucs_auth_ldap_provider_group_name"" -Realm ""ldap"" -Use2Factor ""no"" -Force")
[Void]$ucsadauth.Add("Complete-UcsTransaction")


############### Create UCS Service Profile Templates

############### Create UCS Service Profiles
[Void]$ucsSvcProfiles.Add($carriage_return)

# Combine the sections to form the output of the powershell script
$outputScript = $scriptOptions +
				$scriptInputs +
				$connectUcs +
				$ucsInitialCleanup +
				$ucsGlobalPolicy +
				$ucsVsan +
				$ucsFiPorts +
				$ucsFirmwarePackages +
				$ucsAllPools +
				$ucsQos +
				$ucsNetControlPolicy +
				$ucsVlans +
				$ucsVnicTemplates +
				$ucsVhbaTemplates +
				$ucsPolicies +
				$ucsCallHome +
				$ucsadauth +
				$ucsSpTemplates +
				$ucsSvcProfiles +
				$ucsRemoveDefaults
				
# output the powershell script
$filename = ("$customer_name $ucs_domain_name $ucs_site_num $ucs_pod_num").replace(" ","_")
$outputScript | Out-File "$cmds_path\$filename.ps1" -Encoding ASCII