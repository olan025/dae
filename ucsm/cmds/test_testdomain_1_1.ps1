if ( (Get-PSSnapin -Name ciscoUcsPs -ErrorAction SilentlyContinue) -eq $null ) {
Add-PSSnapin ciscoUcsPs
}

$ErrorActionPreference = "Stop"

$PSVersion = $psversiontable.psversion
$PSMinimum = $PSVersion.Major
if ($PSMinimum -lt "3") {
	Write-Output -ForegroundColor Red "This script requires PowerShell version 3 or above, You are running version . Please update your system and try again."
}

#Ensure all UCS Domains are disconnected
Disconnect-Ucs | Out-Null
$secureUcsAdminPassword = "01000000d08c9ddf0115d1118c7a00c04fc297eb01000000bbc25cfe3331f9429ae13b00149abcb30000000002000000000003660000c00000001000000063efc4086290d1fdbaa3a94dd5e1c44f0000000004800000a00000001000000045b415c1db1193394537ad1594abd986180000000ee762d1ee3a1df8bfb7ea16afc1568b649ef6904a0735fc1400000085c06f3c4ec20b255ff77d8f6fe248d0b0e4f778" | ConvertTo-SecureString
$ucsUsername = "root"
$ucsCredentials = New-Object System.Management.Automation.PSCredential -ArgumentList $ucsUsername, $secureUcsAdminPassword

Write-Output "Logging into UCS Domain"
$myCon = 
$myCon = Connect-Ucs 1.1.1.100 -Credential $ucsCredentials
sleep 1
$myCon = (Get-UcsPSSession | measure).Count
if ($myCon -eq 0) {
	Write-Output -ForegroundColor Red "Logon to UCS domain failed. Please check its availability and try again."
	Disconnect-Ucs
	stop
}

Write-Host "Performing initial cleanup"
Start-UcsTransaction
Get-UcsServerPool | Remove-UcsServerPool -Force
Get-UcsUuidSuffixPool | Remove-UcsUuidSuffixPool -Force
Get-UcsMacPool | Remove-UcsMacPool -Force
Get-UcsVnicTemplate | Remove-UcsVnicTemplate -Force
Get-UcsIpPool | Where-Object {.name -ne "ext-mgmt"} | Remove-UcsIpPool -Force
Get-UcsServiceProfile | Remove-UcsServiceProfile -Force
Get-UcsLdapProvider | Remove-UcsLdapProvider -Force
Get-UcsProviderGroup | Remove-UcsProviderGroup -Force
Get-UcsLanCloud | Get-UcsVlan | Where-Object {.name -ne "default"} | Remove-UcsVlan -Force
Get-UcsVhbaTemplate | Remove-UcsVhbaTemplate -Force
Get-UcsPowerPolicy | Remove-UcsPowerPolicy -Force
Get-UcsMaintenancePolicy | Where-Object {.name -ne "default"} | Remove-UcsMaintenancePolicy -Force
Get-UcsLocalDiskConfigPolicy | Remove-UcsLocalDiskConfigPolicy -Force
Get-UcsBootPolicy | Remove-UcsBootPolicy -Force
Get-UcsBiosPolicy | Remove-UcsBiosPolicy -Force
Get-UcsQosPolicy | Remove-UcsQosPolicy -Force
Get-UcsOrg | Where-Object {.Name -ne "root"} | Remove-UcsOrg -Force
Complete-UcsTransaction

# Set Global System Policies
Start-UcsTransaction
$connPolicy = Get-UcsChassisDiscoveryPolicy | Set-UcsChassisDiscoveryPolicy -Action 4-link -LinkAggregationPref port-channel -Rebalance user-acknowledged -force
$powerPolicy = Get-UcsPowerControlPolicy | Set-UcsPowerControlPolicy -Redundancy grid -force
$macAging = Get-UcsLanCloud | Set-UcsLanCloud -macaging mode-default -force
$powerMgmtPolicy = Get-UcsPowerMgmtPolicy | Set-UcspowerMgmtPolicy -Style manual-per-blade -force
$ucsDns_1 = Add-UcsDnsServer -Name 1.1.1.8
$ucsDns_2 = Add-UcsDnsServer -Name 1.1.1.9
$ucsTimezone = Set-UcsTimezone -Timezone  -Force
$ucsNtp_1 = Add-UcsNtpServer -Name 1.1.1.8
$ucsNtp_2 = Add-UcsNtpServer -Name 1.1.1.9
Complete-UcsTransaction
Start-UcsTransaction
Get-UcsFabricSanCloud -Id "A" | Add-UcsVsan -FcoeVlan  -Id 100 -Name 100
Get-UcsFabricSanCloud -Id "B" | Add-UcsVsan -FcoeVlan  -Id 101 -Name 101
Complete-UcsTransaction

Write-Host "Configuring UCS Server Ports"
Start-UcsTransaction
$mo_1 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 1 -SlotId 1
$mo_2 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 2 -SlotId 1
$mo_3 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 3 -SlotId 1
$mo_4 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 4 -SlotId 1
$mo_5 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 5 -SlotId 1
$mo_6 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 6 -SlotId 1
$mo_7 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 7 -SlotId 1
$mo_8 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 8 -SlotId 1
$mo_9 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 9 -SlotId 1
$mo_10 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 10 -SlotId 1
$mo_11 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 11 -SlotId 1
$mo_12 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 12 -SlotId 1
$mo_13 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 13 -SlotId 1
$mo_14 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 14 -SlotId 1
$mo_15 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 15 -SlotId 1
$mo_16 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 16 -SlotId 1
$mo_17 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 17 -SlotId 1
$mo_18 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 18 -SlotId 1
$mo_19 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 19 -SlotId 1
$mo_20 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 20 -SlotId 1
$mo_21 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 21 -SlotId 1
$mo_22 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 22 -SlotId 1
$mo_23 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 23 -SlotId 1
$mo_24 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 24 -SlotId 1
$mo_25 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 25 -SlotId 1
$mo_26 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 26 -SlotId 1
$mo_27 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 27 -SlotId 1
$mo_28 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 28 -SlotId 1
$mo_29 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 29 -SlotId 1
$mo_30 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 30 -SlotId 1
$mo_31 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 31 -SlotId 1
$mo_32 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 32 -SlotId 1
Complete-UcsTransaction
Start-UcsTransaction
$mo_1 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 1 -SlotId 1
$mo_2 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 2 -SlotId 1
$mo_3 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 3 -SlotId 1
$mo_4 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 4 -SlotId 1
$mo_5 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 5 -SlotId 1
$mo_6 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 6 -SlotId 1
$mo_7 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 7 -SlotId 1
$mo_8 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 8 -SlotId 1
$mo_9 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 9 -SlotId 1
$mo_10 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 10 -SlotId 1
$mo_11 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 11 -SlotId 1
$mo_12 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 12 -SlotId 1
$mo_13 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 13 -SlotId 1
$mo_14 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 14 -SlotId 1
$mo_15 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 15 -SlotId 1
$mo_16 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 16 -SlotId 1
$mo_17 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 17 -SlotId 1
$mo_18 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 18 -SlotId 1
$mo_19 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 19 -SlotId 1
$mo_20 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 20 -SlotId 1
$mo_21 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 21 -SlotId 1
$mo_22 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 22 -SlotId 1
$mo_23 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 23 -SlotId 1
$mo_24 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 24 -SlotId 1
$mo_25 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 25 -SlotId 1
$mo_26 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 26 -SlotId 1
$mo_27 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 27 -SlotId 1
$mo_28 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 28 -SlotId 1
$mo_29 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 29 -SlotId 1
$mo_30 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 30 -SlotId 1
$mo_31 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 31 -SlotId 1
$mo_32 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 32 -SlotId 1
Complete-UcsTransaction

Write-Host "Configuring UCS Ethernet Uplink Ports"
Start-UcsTransaction
$mo = Get-UcsFiLanCloud -Id "A" | Add-UcsUplinkPortChannel -AdminState "enabled" -Name ("vPC" + 30) -PortId 30
$mo_1 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 40 -SlotId 1
$mo_2 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 41 -SlotId 1
$mo_3 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 42 -SlotId 1
$mo_4 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 43 -SlotId 1
$mo_5 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 44 -SlotId 1
Complete-UcsTransaction
Start-UcsTransaction
$mo = Get-UcsFiLanCloud -Id "B" | Add-UcsUplinkPortChannel -AdminState "enabled" -Name ("vPC" + 40) -PortId 40
$mo_1 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 40 -SlotId 1
$mo_2 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 41 -SlotId 1
$mo_3 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 42 -SlotId 1
$mo_4 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 43 -SlotId 1
$mo_5 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 44 -SlotId 1
Complete-UcsTransaction

Write-Host "Configuring UCS FC Uplink Ports"
Start-UcsTransaction
$mo = Get-UcsFabricSanCloud -Id "A" | Add-UcsFcUplinkPortChannel -AdminState "enabled" -Name ("sanPC" + 10) -PortId 10
$mo_1 = $moA | Get-UcsFiSanCloud -Id "A" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 45 -SlotId 1
$mo_2 = $moA | Get-UcsFiSanCloud -Id "A" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 46 -SlotId 1
$mo_3 = $moA | Get-UcsFiSanCloud -Id "A" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 47 -SlotId 1
$mo_4 = $moA | Get-UcsFiSanCloud -Id "A" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 48 -SlotId 1
$addVsanPort = Get-UcsFabricSanCloud -Id "A" | Get-UcsVsan -Name 100 | Add-UcsVsanMemberFcPortChannel -ModifyPresent -AdminState "enabled" -PortId 10 -SwitchId A
Complete-UcsTransaction
Start-UcsTransaction
$mo = Get-UcsFabricSanCloud -Id "B" | Add-UcsFcUplinkPortChannel -AdminState "enabled" -Name ("sanPC" + 210) -PortId 210
$mo_1 = $moB | Get-UcsFiSanCloud -Id "B" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 45 -SlotId 1
$mo_2 = $moB | Get-UcsFiSanCloud -Id "B" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 46 -SlotId 1
$mo_3 = $moB | Get-UcsFiSanCloud -Id "B" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 47 -SlotId 1
$mo_4 = $moB | Get-UcsFiSanCloud -Id "B" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 48 -SlotId 1
$addVsanPort = Get-UcsFabricSanCloud -Id "B" | Get-UcsVsan -Name 101 | Add-UcsVsanMemberFcPortChannel -ModifyPresent -AdminState "enabled" -PortId 210 -SwitchId B
Complete-UcsTransaction


Start-UcsTransaction
$mac1 = Get-UcsOrg -Level root | Add-UcsMacPool -Descr "MAC Address Pool for esxi on fabric A" -Name esxi
$mac = $mac1 | Add-UcsMacMemberBlock -From "00:25:B5:11:A0:00" -To "00:25:B5:11:A0:FF" 
$mac2 = Get-UcsOrg -Level root | Add-UcsMacPool -Descr "MAC Address Pool for esxi on fabric A" -Name esxi
$mac = $mac2 | Add-UcsMacMemberBlock -From "00:25:B5:11:B0:00" -To "00:25:B5:11:B0:FF" 
$wwpn3 = Get-UcsOrg -Level root | Add-UcsWwnPool -Descr "wwpn WWPN Pool on fabric A" -Name  -Purpose port-wwn-assignment
$wwpn = $wwpn3 | Add-UcsWwnMemberBlock -From "20:00:00:25:B5:11:A0:00" -To "20:00:00:25:B5:11:A0:FF" 
$wwpn4 = Get-UcsOrg -Level root | Add-UcsWwnPool -Descr "wwpn WWPN Pool on fabric B" -Name  -Purpose port-wwn-assignment
$wwpn = $wwpn4 | Add-UcsWwnMemberBlock -From "20:00:00:25:B5:11:B0:00" -To "20:00:00:25:B5:11:B0:FF" 
$wwnn1 = Get-UcsOrg -Level root | Add-UcsWwnPool -Descr "WWNN Pool" -Name  -Purpose node-wwn-assignment
$wwnn1_1 = $wwnn1 | Add-UcsWwnMemberBlock -From "20:00:00:25:B5:11:B0:00" -To "20:00:00:25:B5:11:B0:FF" 
$uuid1 = Get-UcsOrg -Level root  | Add-UcsUuidSuffixPool -Descr "UUID Pool for Server system board IDs" -Name UUID -Prefix derived
$uuid1_1 = $uuid1 | Add-UcsUuidSuffixBlock -From 0101-110000000001 -To 0101-000000000200
Complete-UcsTransaction

Start-UcsTransaction
Set-UcsQosClass -QosClass gold -Weight 4 -Mtu 9216 -AdminState enabled -Force
Set-UcsQosClass -QosClass silver -Weight 3 -mtu 9216 -AdminState enabled -Force
Set-UcsQosClass -QosClass bronze -Weight 2 -mtu 9216 -AdminState enabled -Force
Set-UcsBestEffortQosClass -Weight 1 -mtu 9216 -Force
Set-UcsFcQosClass -Weight 0 -Force
Complete-UcsTransaction

Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root | Add-UcsQosPolicy -Name "ESXi_FC"
$mo = $mo1 | Add-UcsVnicEgressPolicy -ModifyPresent -Burst 10240 -HostControl none -Prio fc -Rate line-rate
$mo2 = Get-UcsOrg -Level root | Add-UcsQosPolicy -Name "ESXi_ETH"
$mo = $mo2 | Add-UcsVnicEgressPolicy -ModifyPresent -Burst 10240 -HostControl full -Prio best-effort -Rate line-rate
Complete-UcsTransaction







Write-Host "Cleaning up UCS default values and pools"
Start-UcsTransaction
Get-UcsServerPool -Name default -LimitScope | Remove-UcsServerPool -Force
Get-UcsUuidSuffixPool -Name default -LimitScope | Remove-UcsUuidSuffixPool -Force
Get-UcsWwnPool -Name node-default -LimitScope | Remove-UcsWwnPool -Force
Get-UcsWwnPool -Name default -LimitScope | Remove-UcsWwnPool -Force
Get-UcsMacPool -Name default -LimitScope | Remove-UcsMacPool -Force
Get-UcsManagedObject -Dn org-root/iqn-pool-default | Remove-UcsManagedObject -Force
Complete-UcsTransaction
