if ( (Get-PSSnapin -Name ciscoUcsPs -ErrorAction SilentlyContinue) -eq $null ) {
Add-PSSnapin ciscoUcsPs
}

$ErrorActionPreference = "Stop"

$PSVersion = $psversiontable.psversion
$PSMinimum = $PSVersion.Major
if ($PSMinimum -lt "3") {
	Write-Output -ForegroundColor Red "This script requires PowerShell version 3 or above, You are running version . Please update your system and try again."
}

#Ensure all UCS Domains are disconnected
Disconnect-Ucs | Out-Null
$secureUcsAdminPassword = "01000000d08c9ddf0115d1118c7a00c04fc297eb0100000070ac505b71d91a47a336162606738a1a0000000002000000000003660000c0000000100000000be756861235bc702f321b8fa3e46c290000000004800000a000000010000000935f5805b7dcf66e715f8bb70e65e6fb18000000acd03f2a02b3e7ea35b06f6a755fc8c16317cbe37382649c14000000e05d7645fd2b9667afe215bbe216cde4d215ea83" | ConvertTo-SecureString
$ucsUsername = "root"
$ucsCredentials = New-Object System.Management.Automation.PSCredential -ArgumentList $ucsUsername, $secureUcsAdminPassword

Write-Output "Logging into UCS Domain"
$myCon = 
$myCon = Connect-Ucs 1.1.1.100 -Credential $ucsCredentials
sleep 1
$myCon = (Get-UcsPSSession | measure).Count
if ($myCon -eq 0) {
	Write-Output -ForegroundColor Red "Logon to UCS domain failed. Please check its availability and try again."
	Disconnect-Ucs
	stop
}

Write-Host "Performing initial cleanup"
Start-UcsTransaction
Get-UcsServerPool | Remove-UcsServerPool -Force
Get-UcsUuidSuffixPool | Remove-UcsUuidSuffixPool -Force
Get-UcsMacPool | Remove-UcsMacPool -Force
Get-UcsVnicTemplate | Remove-UcsVnicTemplate -Force
Get-UcsIpPool | Where-Object {$_.name -ne "ext-mgmt"} | Remove-UcsIpPool -Force
Get-UcsSanCloud | Get-UcsVsan | Where-Object {$_.name -ne "default"} | Remove-UcsVsan -Force
Get-UcsServiceProfile | Remove-UcsServiceProfile -Force
Get-UcsLdapProvider | Remove-UcsLdapProvider -Force
Get-UcsProviderGroup | Remove-UcsProviderGroup -Force
Get-UcsLanCloud | Get-UcsVlan | Where-Object {$_.name -ne "default"} | Remove-UcsVlan -Force
Get-UcsVhbaTemplate | Remove-UcsVhbaTemplate -Force
Get-UcsPowerPolicy | Remove-UcsPowerPolicy -Force
Get-UcsMaintenancePolicy | Where-Object {$_.name -ne "default"} | Remove-UcsMaintenancePolicy -Force
Get-UcsLocalDiskConfigPolicy | Remove-UcsLocalDiskConfigPolicy -Force
Get-UcsBootPolicy | Remove-UcsBootPolicy -Force
Get-UcsBiosPolicy | Remove-UcsBiosPolicy -Force
Get-UcsQosPolicy | Remove-UcsQosPolicy -Force
Get-UcsOrg | Where-Object {$_.Name -ne "root"} | Remove-UcsOrg -Force
Get-UcsDnsServer | Remove-UcsDnsServer -Force
Get-UcsNtpServer | Remove-UcsNtpServer -Force
Get-UcsNetworkControlPolicy | Remove-UcsNetworkControlPolicy -Force
Get-UcsWwnPool | Remove-UcsWwnPool -Force
Complete-UcsTransaction

# Set Global System Policies
Start-UcsTransaction
$connPolicy = Get-UcsChassisDiscoveryPolicy | Set-UcsChassisDiscoveryPolicy -Action 4-link -LinkAggregationPref port-channel -Rebalance user-acknowledged -force
$powerPolicy = Get-UcsPowerControlPolicy | Set-UcsPowerControlPolicy -Redundancy grid -force
$macAging = Get-UcsLanCloud | Set-UcsLanCloud -macaging mode-default -force
$powerMgmtPolicy = Get-UcsPowerMgmtPolicy | Set-UcspowerMgmtPolicy -Style manual-per-blade -force
$ucsDns_1 = Add-UcsDnsServer -Name 1.1.1.8
$ucsDns_2 = Add-UcsDnsServer -Name 1.1.1.9
$ucsTimezone = Set-UcsTimezone -Timezone "America/Chicago (Central Time)" -Force
$ucsNtp_1 = Add-UcsNtpServer -Name 1.1.1.8
$ucsNtp_2 = Add-UcsNtpServer -Name 1.1.1.9
Complete-UcsTransaction
Start-UcsTransaction
Get-UcsSanCloud -Id "A" | Add-UcsVsan -FcoeVlan  -Id 100 -Name 100
Get-UcsSanCloud -Id "B" | Add-UcsVsan -FcoeVlan  -Id 101 -Name 101
Complete-UcsTransaction

Write-Host "Configuring UCS Server Ports"
Start-UcsTransaction
$mo_1 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 9 -SlotId 1
$mo_2 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 10 -SlotId 1
$mo_3 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 11 -SlotId 1
$mo_4 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 12 -SlotId 1
Complete-UcsTransaction
Start-UcsTransaction
$mo_1 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 9 -SlotId 1
$mo_2 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 10 -SlotId 1
$mo_3 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 11 -SlotId 1
$mo_4 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 12 -SlotId 1
Complete-UcsTransaction

Write-Host "Configuring UCS Ethernet Uplink Ports"
Start-UcsTransaction
$mo = Get-UcsFiLanCloud -Id "A" | Add-UcsUplinkPortChannel -AdminState "enabled" -Name ("vPC" + 30) -PortId 30
$mo_1 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 40 -SlotId 1
$mo_2 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 41 -SlotId 1
$mo_3 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 42 -SlotId 1
$mo_4 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 43 -SlotId 1
$mo_5 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 44 -SlotId 1
Complete-UcsTransaction
Start-UcsTransaction
$mo = Get-UcsFiLanCloud -Id "B" | Add-UcsUplinkPortChannel -AdminState "enabled" -Name ("vPC" + 40) -PortId 40
$mo_1 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 40 -SlotId 1
$mo_2 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 41 -SlotId 1
$mo_3 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 42 -SlotId 1
$mo_4 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 43 -SlotId 1
$mo_5 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState "enabled" -PortId 44 -SlotId 1
Complete-UcsTransaction

Write-Host "Configuring UCS FC Uplink Ports"
Start-UcsTransaction
$mo = Get-UcsSanCloud -Id "A" | Add-UcsFcUplinkPortChannel -AdminState "enabled" -Name ("sanPC" + 10) -PortId 10
$mo_1 = $moA | Get-UcsFiSanCloud -Id "A" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 45 -SlotId 1
$mo_2 = $moA | Get-UcsFiSanCloud -Id "A" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 46 -SlotId 1
$mo_3 = $moA | Get-UcsFiSanCloud -Id "A" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 47 -SlotId 1
$mo_4 = $moA | Get-UcsFiSanCloud -Id "A" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 48 -SlotId 1
$addVsanPort = Get-UcsSanCloud -Id "A" | Get-UcsVsan -Name 100 | Add-UcsVsanMemberFcPortChannel -ModifyPresent -AdminState "enabled" -PortId 10 -SwitchId A
Complete-UcsTransaction
Start-UcsTransaction
$mo = Get-UcsSanCloud -Id "B" | Add-UcsFcUplinkPortChannel -AdminState "enabled" -Name ("sanPC" + 210) -PortId 210
$mo_1 = $moB | Get-UcsFiSanCloud -Id "B" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 45 -SlotId 1
$mo_2 = $moB | Get-UcsFiSanCloud -Id "B" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 46 -SlotId 1
$mo_3 = $moB | Get-UcsFiSanCloud -Id "B" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 47 -SlotId 1
$mo_4 = $moB | Get-UcsFiSanCloud -Id "B" | Add-UcsFcUplinkPort -ModifyPresent -AdminState "enabled" -PortId 48 -SlotId 1
$addVsanPort = Get-UcsSanCloud -Id "B" | Get-UcsVsan -Name 101 | Add-UcsVsanMemberFcPortChannel -ModifyPresent -AdminState "enabled" -PortId 210 -SwitchId B
Complete-UcsTransaction


Start-UcsTransaction
$mac1 = Get-UcsOrg -Level root | Add-UcsMacPool -AssignmentOrder "sequential" -Descr "MAC Address Pool on Fabric-A" -Name Fabric-A
$mac = $mac1 | Add-UcsMacMemberBlock -From "00:25:B5:11:A0:00" -To "00:25:B5:11:A0:FF" 
$mac2 = Get-UcsOrg -Level root | Add-UcsMacPool -AssignmentOrder "sequential" -Descr "MAC Address Pool on Fabric-B" -Name Fabric-B
$mac = $mac2 | Add-UcsMacMemberBlock -From "00:25:B5:11:B0:00" -To "00:25:B5:11:B0:FF" 
$wwpn3 = Get-UcsOrg -Level root | Add-UcsWwnPool -AssignmentOrder "sequential" -Descr "WWPN Pool on fabric Fabric-A" -Name Fabric-A -Purpose port-wwn-assignment
$wwpn = $wwpn3 | Add-UcsWwnMemberBlock -From "20:00:00:25:B5:11:A0:00" -To "20:00:00:25:B5:11:A0:FF" 
$wwpn4 = Get-UcsOrg -Level root | Add-UcsWwnPool -AssignmentOrder "sequential" -Descr "WWPN Pool on fabric Fabric-B" -Name Fabric-B -Purpose port-wwn-assignment
$wwpn = $wwpn4 | Add-UcsWwnMemberBlock -From "20:00:00:25:B5:11:B0:00" -To "20:00:00:25:B5:11:B0:FF" 
$wwnn1 = Get-UcsOrg -Level root | Add-UcsWwnPool -AssignmentOrder "sequential" -Descr "WWNN Pool" -Name "Global" -Purpose node-wwn-assignment
$wwnn1_1 = $wwnn1 | Add-UcsWwnMemberBlock -From "20:00:00:25:B5:11:B0:00" -To "20:00:00:25:B5:11:B0:FF" 
$uuid1 = Get-UcsOrg -Level root  | Add-UcsUuidSuffixPool -Descr "UUID Pool for Server system board IDs" -Name UUID -Prefix derived
$uuid1_1 = $uuid1 | Add-UcsUuidSuffixBlock -From 0101-110000000001 -To 0101-000000000200
Complete-UcsTransaction

Start-UcsTransaction
Set-UcsQosClass -QosClass gold -Weight 4 -Mtu 9216 -AdminState enabled -Force
Set-UcsQosClass -QosClass silver -Weight 3 -mtu 9216 -AdminState enabled -Force
Set-UcsQosClass -QosClass bronze -Weight 2 -mtu 9216 -AdminState enabled -Force
Set-UcsBestEffortQosClass -Weight 1 -mtu 9216 -Force
Set-UcsFcQosClass -Weight 0 -Force
Complete-UcsTransaction

Start-UcsTransaction
Complete-UcsTransaction

Start-UcsTransaction
$mo = Get-UcsOrg -Level root | Add-UcsNetworkControlPolicy -Cdp "enabled" -Descr "" -MacRegisterMode "only-native-vlan" -Name "CDP-Enabled" -PolicyOwner "local" -UplinkFailAction "link-down"
$mo_1 = $mo | Add-UcsPortSecurityConfig -ModifyPresent -Descr "" -Forge "allow" -Name "" -PolicyOwner "local"
Complete-UcsTransaction

Write-Host "Creating UCS VLANs"
Start-UcsTransaction
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 210 -Name 210_PhyMgmt
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 211 -Name 211_VirtualMgmt
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 212 -Name 212_ISCSI
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 213 -Name 213_vmotion
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 214 -Name 214_anyconnect
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 215 -Name 215_UC
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 320 -Name 320_DMZ
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 322 -Name 322_Gen_App
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 324 -Name 324_VDI
Complete-UcsTransaction

Write-Host "Creating UCS vNic Templates"
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic0 -IdentPoolName Fabric-A -Mtu 9000 -Name esxi-mgmt-a -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Best-Effort -StatsPolicyName "default" -SwitchId A -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 210_PhyMgmt
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic1 -IdentPoolName Fabric-B -Mtu 9000 -Name esxi-mgmt-b -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Best-Effort -StatsPolicyName "default" -SwitchId B -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 210_PhyMgmt
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic2 -IdentPoolName Fabric-A -Mtu 9000 -Name esxi-vmo-a -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Bronze -StatsPolicyName "default" -SwitchId A -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 213_vmotion
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic3 -IdentPoolName Fabric-B -Mtu 9000 -Name esxi-vmo-b -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Bronze -StatsPolicyName "default" -SwitchId B -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 213_vmotion
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic4 -IdentPoolName Fabric-A -Mtu 9000 -Name esxi-nas-a -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Gold -StatsPolicyName "default" -SwitchId A -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 212_ISCSI
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic5 -IdentPoolName Fabric-B -Mtu 9000 -Name esxi-nas-b -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Gold -StatsPolicyName "default" -SwitchId B -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 212_ISCSI
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic6 -IdentPoolName Fabric-A -Mtu 9000 -Name esxi-guest-a -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Silver -StatsPolicyName "default" -SwitchId A -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 322_Gen_App
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic7 -IdentPoolName Fabric-B -Mtu 9000 -Name esxi-guest-b -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Silver -StatsPolicyName "default" -SwitchId B -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 322_Gen_App
Complete-UcsTransaction

Start-UcsTransaction
Add-UcsVnicInterface -VnicTemplate esxi-guest-a -ModifyPresent -DefaultNet no -Name 214_anyconnect
Add-UcsVnicInterface -VnicTemplate esxi-guest-a -ModifyPresent -DefaultNet no -Name 215_UC
Add-UcsVnicInterface -VnicTemplate esxi-guest-a -ModifyPresent -DefaultNet no -Name 320_DMZ
Add-UcsVnicInterface -VnicTemplate esxi-guest-a -ModifyPresent -DefaultNet no -Name 324_VDI
Add-UcsVnicInterface -VnicTemplate esxi-guest-b -ModifyPresent -DefaultNet no -Name 214_anyconnect
Add-UcsVnicInterface -VnicTemplate esxi-guest-b -ModifyPresent -DefaultNet no -Name 215_UC
Add-UcsVnicInterface -VnicTemplate esxi-guest-b -ModifyPresent -DefaultNet no -Name 320_DMZ
Add-UcsVnicInterface -VnicTemplate esxi-guest-b -ModifyPresent -DefaultNet no -Name 324_VDI
Complete-UcsTransaction

Start-UcsTransaction
$mo = Get-UcsOrg -Level root  | Add-UcsVhbaTemplate -Descr VMware ESXi vHBA 0 for Fabric A -IdentPoolName "Global" -MaxDataFieldSize 2048 -Name  -QosPolicyName ESXi_FC -StatsPolicyName default -SwitchId A -TemplType updating-template
$mo_1 = $mo | Add-UcsVhbaInterface -ModifyPresent -Name 
$mo = Get-UcsOrg -Level root  | Add-UcsVhbaTemplate -Descr VMware ESXi vHBA 1 for Fabric B -IdentPoolName "Global" -MaxDataFieldSize 2048 -Name  -QosPolicyName ESXi_FC -StatsPolicyName default -SwitchId A -TemplType updating-template
$mo_1 = $mo | Add-UcsVhbaInterface -ModifyPresent -Name 
Complete-UcsTransaction

Write-Host "Configuring UCS Policies"
Get-UcsOrg -Level root  | Add-UcsLocalDiskConfigPolicy -Descr "" -FlexFlashRAIDReportingState "enable" -FlexFlashState "enable" -Mode "any-configuration" -Name "SD" -PolicyOwner "local" -ProtectConfig "yes"
Get-UcsOrg -Level root  | Add-UcsLocalDiskConfigPolicy -Descr "" -FlexFlashRAIDReportingState "disable" -FlexFlashState "disable" -Mode "raid-mirrored" -Name "Local_Disk" -PolicyOwner "local" -ProtectConfig "yes"
Get-UcsOrg -Level root | Get-UcsMaintenancePolicy -Name "default" -LimitScope | Set-UcsMaintenancePolicy -Descr "" -SchedName "" -UptimeDisr "user-ack" -Force
Get-UcsOrg -Level root | Add-UcsMaintenancePolicy -Descr "" -Name "User-Acknowledge" -SchedName "" -UptimeDisr "user-ack"
Get-UcsOrg -Level root | Get-UcsScrubPolicy -Name "default" -LimitScope | Set-UcsScrubPolicy -BiosSettingsScrub "yes" -Descr "" -DiskScrub "no" -FlexFlashScrub "no" -PolicyOwner "local" -Force
Get-UcsOrg -Level root  | Add-UcsScrubPolicy -BiosSettingsScrub "yes" -Descr "" -DiskScrub "no" -FlexFlashScrub "no" -Name "Bios-Only" -PolicyOwner "local"



Write-Host "Cleaning up UCS default values and pools"
Start-UcsTransaction
Get-UcsServerPool -Name default -LimitScope | Remove-UcsServerPool -Force
Get-UcsUuidSuffixPool -Name default -LimitScope | Remove-UcsUuidSuffixPool -Force
Get-UcsWwnPool -Name node-default -LimitScope | Remove-UcsWwnPool -Force
Get-UcsWwnPool -Name default -LimitScope | Remove-UcsWwnPool -Force
Get-UcsMacPool -Name default -LimitScope | Remove-UcsMacPool -Force
Get-UcsManagedObject -Dn org-root/iqn-pool-default | Remove-UcsManagedObject -Force
Complete-UcsTransaction
