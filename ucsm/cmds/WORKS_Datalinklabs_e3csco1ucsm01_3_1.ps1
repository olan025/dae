#if ( (Get-PSSnapin -Name ciscoUcsPs -ErrorAction SilentlyContinue) -eq $null ) {
#Add-PSSnapin ciscoUcsPs
#}

$ErrorActionPreference = "Stop"

$PSVersion = $psversiontable.psversion
$PSMinimum = $PSVersion.Major
if ($PSMinimum -lt "3") {
	Write-Output -ForegroundColor Red "This script requires PowerShell version 3 or above, You are running version . Please update your system and try again."
}

#Ensure all UCS Domains are disconnected
Disconnect-Ucs | Out-Null
$secureUcsAdminPassword = "01000000d08c9ddf0115d1118c7a00c04fc297eb0100000070ac505b71d91a47a336162606738a1a0000000002000000000003660000c00000001000000066d3ecb87d677aa3f5061ef463955dbd0000000004800000a0000000100000006d1a0a890da820fc34b75b0fc2d9212d1800000076f78b62e09b6f139c0df3839b6610d71bdc73f87baae5a614000000379729bb9642ff9027701a4eb7238543df3f3bd6" | ConvertTo-SecureString
$ucsUsername = "admin"
$ucsCredentials = New-Object System.Management.Automation.PSCredential -ArgumentList $ucsUsername, $secureUcsAdminPassword

Write-Output "Logging into UCS Domain"
$myCon = 
$myCon = Connect-Ucs 10.203.11.50 -Credential $ucsCredentials
sleep 1
$myCon = (Get-UcsPSSession | measure).Count
if ($myCon -eq 0) {
	Write-Output -ForegroundColor Red "Logon to UCS domain failed. Please check its availability and try again."
	Disconnect-Ucs
	stop
}

Write-Host "Performing initial cleanup"
Start-UcsTransaction
Get-UcsServerPool | Remove-UcsServerPool -Force
Get-UcsUuidSuffixPool | Remove-UcsUuidSuffixPool -Force
Get-UcsMacPool | Remove-UcsMacPool -Force
Get-UcsVnicTemplate | Remove-UcsVnicTemplate -Force
Get-UcsIpPool | Where-Object {$_.name -ne "ext-mgmt"} | Remove-UcsIpPool -Force
Get-UcsSanCloud | Get-UcsVsan | Where-Object {$_.name -ne "default"} | Remove-UcsVsan -Force
Get-UcsServiceProfile | Remove-UcsServiceProfile -Force
Get-UcsLdapProvider | Remove-UcsLdapProvider -Force
Get-UcsProviderGroup | Remove-UcsProviderGroup -Force
Get-UcsLanCloud | Get-UcsVlan | Where-Object {$_.name -ne "default"} | Remove-UcsVlan -Force
Get-UcsVhbaTemplate | Remove-UcsVhbaTemplate -Force
Get-UcsPowerPolicy | Remove-UcsPowerPolicy -Force
Get-UcsMaintenancePolicy | Where-Object {$_.name -ne "default"} | Remove-UcsMaintenancePolicy -Force
Get-UcsLocalDiskConfigPolicy | Remove-UcsLocalDiskConfigPolicy -Force
Get-UcsBootPolicy | Remove-UcsBootPolicy -Force
Get-UcsBiosPolicy | Remove-UcsBiosPolicy -Force
Get-UcsQosPolicy | Remove-UcsQosPolicy -Force
Get-UcsOrg | Where-Object {$_.Name -ne "root"} | Remove-UcsOrg -Force
Get-UcsDnsServer | Remove-UcsDnsServer -Force
Get-UcsNtpServer | Remove-UcsNtpServer -Force
Get-UcsNetworkControlPolicy | Remove-UcsNetworkControlPolicy -Force
Get-UcsWwnPool | Remove-UcsWwnPool -Force
Complete-UcsTransaction

# Set Global System Policies
Start-UcsTransaction
$connPolicy = Get-UcsChassisDiscoveryPolicy | Set-UcsChassisDiscoveryPolicy -Action 4-link -LinkAggregationPref port-channel -Rebalance user-acknowledged -force
$powerPolicy = Get-UcsPowerControlPolicy | Set-UcsPowerControlPolicy -Redundancy grid -force
$macAging = Get-UcsLanCloud | Set-UcsLanCloud -macaging mode-default -force
$powerMgmtPolicy = Get-UcsPowerMgmtPolicy | Set-UcspowerMgmtPolicy -Style manual-per-blade -force
$ucsDns_1 = Add-UcsDnsServer -Name 10.201.60.10
$ucsDns_2 = Add-UcsDnsServer -Name 10.201.60.11
$ucsTimezone = Set-UcsTimezone -Timezone "America/Chicago (Central Time)" -Force
$ucsNtp_1 = Add-UcsNtpServer -Name time.datalinklabs.com
Complete-UcsTransaction
Start-UcsTransaction
Get-UcsFiSanCloud -Id "A" | Add-UcsVsan -FcZoneSharingMode "coalesce" -FcoeVlan 31 -Id 31 -Name "31" -PolicyOwner "local" -ZoningState "disabled"
Get-UcsFiSanCloud -Id "B" | Add-UcsVsan -FcZoneSharingMode "coalesce" -FcoeVlan 32 -Id 32 -Name "32" -PolicyOwner "local" -ZoningState "disabled"
Complete-UcsTransaction

#Write-Host "Configuring UCS Server Ports"
#Start-UcsTransaction
#$mo_1 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 9 -SlotId 1
#$mo_2 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 10 -SlotId 1
#$mo_3 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 11 -SlotId 1
#$mo_4 = Get-UcsFabricServerCloud -Id "A" | Add-UcsServerPort -AdminState "enabled" -PortId 12 -SlotId 1
#Complete-UcsTransaction
#Start-UcsTransaction
#$mo_1 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 9 -SlotId 1
#$mo_2 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 10 -SlotId 1
#$mo_3 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 11 -SlotId 1
#$mo_4 = Get-UcsFabricServerCloud -Id "B" | Add-UcsServerPort -AdminState "enabled" -PortId 12 -SlotId 1
#Complete-UcsTransaction

Start-UcsTransaction
$mo = Get-UcsFiSanCloud -Id "A" | Add-UcsFcUplinkPortChannel -ModifyPresent  -AdminSpeed "auto" -AdminState "enabled" -Descr "" -Name "sanPC1" -PortId 1
$mo_1 = $mo | Add-UcsFabricFcSanPcEp -AdminSpeed "auto" -AdminState "enabled" -FillPattern "arbff" -Name "" -PortId 1 -SlotId 2
$mo_2 = $mo | Add-UcsFabricFcSanPcEp -AdminSpeed "auto" -AdminState "enabled" -FillPattern "arbff" -Name "" -PortId 2 -SlotId 2
$mo_3 = $mo | Add-UcsFabricFcSanPcEp -AdminSpeed "auto" -AdminState "enabled" -FillPattern "arbff" -Name "" -PortId 3 -SlotId 2
$mo_4 = $mo | Add-UcsFabricFcSanPcEp -AdminSpeed "auto" -AdminState "enabled" -FillPattern "arbff" -Name "" -PortId 4 -SlotId 2
Complete-UcsTransaction






Start-UcsTransaction
$mac1 = Get-UcsOrg -Level root | Add-UcsMacPool -AssignmentOrder "sequential" -Descr "MAC Address Pool on Fabric-A" -Name Fabric-A
$mac = $mac1 | Add-UcsMacMemberBlock -From "00:25:B5:31:A0:00" -To "00:25:B5:31:A0:FF" 
$mac2 = Get-UcsOrg -Level root | Add-UcsMacPool -AssignmentOrder "sequential" -Descr "MAC Address Pool on Fabric-B" -Name Fabric-B
$mac = $mac2 | Add-UcsMacMemberBlock -From "00:25:B5:31:B0:00" -To "00:25:B5:31:B0:FF" 
$wwpn3 = Get-UcsOrg -Level root | Add-UcsWwnPool -AssignmentOrder "sequential" -Descr "WWPN Pool on fabric Fabric-A" -Name Fabric-A -Purpose port-wwn-assignment
$wwpn = $wwpn3 | Add-UcsWwnMemberBlock -From "20:00:00:25:B5:31:A0:00" -To "20:00:00:25:B5:31:A0:FF" 
$wwpn4 = Get-UcsOrg -Level root | Add-UcsWwnPool -AssignmentOrder "sequential" -Descr "WWPN Pool on fabric Fabric-B" -Name Fabric-B -Purpose port-wwn-assignment
$wwpn = $wwpn4 | Add-UcsWwnMemberBlock -From "20:00:00:25:B5:31:B0:00" -To "20:00:00:25:B5:31:B0:FF" 
$wwnn1 = Get-UcsOrg -Level root | Add-UcsWwnPool -AssignmentOrder "sequential" -Descr "WWNN Pool" -Name "Global" -Purpose node-wwn-assignment
$wwnn1_1 = $wwnn1 | Add-UcsWwnMemberBlock -From "20:00:00:25:B5:31:B0:00" -To "20:00:00:25:B5:31:B0:FF" 
$uuid1 = Get-UcsOrg -Level root  | Add-UcsUuidSuffixPool -Descr "UUID Pool for Server system board IDs" -Name UUID -Prefix derived
$uuid1_1 = $uuid1 | Add-UcsUuidSuffixBlock -From 0301-000000000001 -To 0301-000000000200
Complete-UcsTransaction

Start-UcsTransaction
Set-UcsQosClass -QosClass gold -Weight 4 -Mtu 9216 -AdminState enabled -Force
Set-UcsQosClass -QosClass silver -Weight 3 -mtu 9216 -AdminState enabled -Force
Set-UcsQosClass -QosClass bronze -Weight 2 -mtu 9216 -AdminState enabled -Force
Set-UcsBestEffortQosClass -Weight 1 -mtu 9216 -Force
Set-UcsFcQosClass -Weight 0 -Force
Complete-UcsTransaction

Start-UcsTransaction
Complete-UcsTransaction

Start-UcsTransaction
$mo = Get-UcsOrg -Level root | Add-UcsNetworkControlPolicy -Cdp "enabled" -Descr "" -MacRegisterMode "only-native-vlan" -Name "CDP-Enabled" -PolicyOwner "local" -UplinkFailAction "link-down"
$mo_1 = $mo | Add-UcsPortSecurityConfig -ModifyPresent -Descr "" -Forge "allow" -Name "" -PolicyOwner "local"
Complete-UcsTransaction

Write-Host "Creating UCS VLANs"
Start-UcsTransaction
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 312 -Name 312_Host-Management
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 330 -Name 330_vMotion
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 333 -Name 333_NAS
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 334 -Name 334_iSCSI-A
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 335 -Name 335_iSCSI-B
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 360 -Name 360_Guest-Windows
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 361 -Name 361_Guest-Linux
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 362 -Name 362_Guest-Other
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id 363 -Name 363_OS-Cluster
Complete-UcsTransaction

Write-Host "Creating UCS vNic Templates"
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic0 -IdentPoolName Fabric-A -Mtu 9000 -Name esxi-mgmt-a -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Best-Effort -StatsPolicyName "default" -SwitchId A -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 312_Host-Management
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic1 -IdentPoolName Fabric-B -Mtu 9000 -Name esxi-mgmt-b -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Best-Effort -StatsPolicyName "default" -SwitchId B -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 312_Host-Management
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic2 -IdentPoolName Fabric-A -Mtu 9000 -Name esxi-vmo-a -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Bronze -StatsPolicyName "default" -SwitchId A -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 330_vMotion
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic3 -IdentPoolName Fabric-B -Mtu 9000 -Name esxi-vmo-b -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Bronze -StatsPolicyName "default" -SwitchId B -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 330_vMotion
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic4 -IdentPoolName Fabric-A -Mtu 9000 -Name esxi-nas-a -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Gold -StatsPolicyName "default" -SwitchId A -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 334_iSCSI-A
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic5 -IdentPoolName Fabric-B -Mtu 9000 -Name esxi-nas-b -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Gold -StatsPolicyName "default" -SwitchId B -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 335_iSCSI-B
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic6 -IdentPoolName Fabric-A -Mtu 9000 -Name esxi-guest-a -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Silver -StatsPolicyName "default" -SwitchId A -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 361_Guest-Linux
Complete-UcsTransaction
Start-UcsTransaction
$mo1 = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr vmnic7 -IdentPoolName Fabric-B -Mtu 9000 -Name esxi-guest-b -NwCtrlPolicyName "Enable_CDP" -PinToGroupName "" -PolicyOwner "local" -QosPolicyName Silver -StatsPolicyName "default" -SwitchId B -TemplType "updating-template"
$mo_1 = $mo1 | Add-UcsVnicInterface -ModifyPresent -DefaultNet "no" -Name 361_Guest-Linux
Complete-UcsTransaction

Start-UcsTransaction
Add-UcsVnicInterface -VnicTemplate esxi-nas-a -ModifyPresent -DefaultNet no -Name 333_NAS
Add-UcsVnicInterface -VnicTemplate esxi-nas-b -ModifyPresent -DefaultNet no -Name 333_NAS
Add-UcsVnicInterface -VnicTemplate esxi-guest-a -ModifyPresent -DefaultNet no -Name 360_Guest-Windows
Add-UcsVnicInterface -VnicTemplate esxi-guest-a -ModifyPresent -DefaultNet no -Name 362_Guest-Other
Add-UcsVnicInterface -VnicTemplate esxi-guest-a -ModifyPresent -DefaultNet no -Name 363_OS-Cluster
Add-UcsVnicInterface -VnicTemplate esxi-guest-b -ModifyPresent -DefaultNet no -Name 360_Guest-Windows
Add-UcsVnicInterface -VnicTemplate esxi-guest-b -ModifyPresent -DefaultNet no -Name 362_Guest-Other
Add-UcsVnicInterface -VnicTemplate esxi-guest-b -ModifyPresent -DefaultNet no -Name 363_OS-Cluster
Complete-UcsTransaction

Start-UcsTransaction
$mo = Get-UcsOrg -Level root  | Add-UcsVhbaTemplate -Descr "VMware ESXi vHBA 0 for Fabric A" -IdentPoolName "Global" -MaxDataFieldSize 2048 -Name vHBA-31 -QosPolicyName ESXi_FC -StatsPolicyName default -SwitchId A -TemplType updating-template
$mo_1 = $mo | Add-UcsVhbaInterface -ModifyPresent -Name 31
$mo = Get-UcsOrg -Level root  | Add-UcsVhbaTemplate -Descr "VMware ESXi vHBA 1 for Fabric B" -IdentPoolName "Global" -MaxDataFieldSize 2048 -Name vHBA-32 -QosPolicyName ESXi_FC -StatsPolicyName default -SwitchId A -TemplType updating-template
$mo_1 = $mo | Add-UcsVhbaInterface -ModifyPresent -Name 32
Complete-UcsTransaction

Write-Host "Configuring UCS Policies"
Get-UcsOrg -Level root  | Add-UcsLocalDiskConfigPolicy -Descr "" -FlexFlashRAIDReportingState "enable" -FlexFlashState "enable" -Mode "any-configuration" -Name "SD" -PolicyOwner "local" -ProtectConfig "yes"
Get-UcsOrg -Level root  | Add-UcsLocalDiskConfigPolicy -Descr "" -FlexFlashRAIDReportingState "disable" -FlexFlashState "disable" -Mode "raid-mirrored" -Name "Local_Disk" -PolicyOwner "local" -ProtectConfig "yes"
Get-UcsOrg -Level root | Get-UcsMaintenancePolicy -Name "default" -LimitScope | Set-UcsMaintenancePolicy -Descr "" -SchedName "" -UptimeDisr "user-ack" -Force
Get-UcsOrg -Level root | Add-UcsMaintenancePolicy -Descr "" -Name "User-Acknowledge" -SchedName "" -UptimeDisr "user-ack"
Get-UcsOrg -Level root | Get-UcsScrubPolicy -Name "default" -LimitScope | Set-UcsScrubPolicy -BiosSettingsScrub "yes" -Descr "" -DiskScrub "no" -FlexFlashScrub "no" -PolicyOwner "local" -Force
Get-UcsOrg -Level root  | Add-UcsScrubPolicy -BiosSettingsScrub "yes" -Descr "" -DiskScrub "no" -FlexFlashScrub "no" -Name "Bios-Only" -PolicyOwner "local"



Write-Host "Cleaning up UCS default values and pools"
Start-UcsTransaction
Get-UcsServerPool -Name default -LimitScope | Remove-UcsServerPool -Force
Get-UcsUuidSuffixPool -Name default -LimitScope | Remove-UcsUuidSuffixPool -Force
Get-UcsWwnPool -Name node-default -LimitScope | Remove-UcsWwnPool -Force
Get-UcsWwnPool -Name default -LimitScope | Remove-UcsWwnPool -Force
Get-UcsMacPool -Name default -LimitScope | Remove-UcsMacPool -Force
Get-UcsManagedObject -Dn org-root/iqn-pool-default | Remove-UcsManagedObject -Force
Complete-UcsTransaction
